$(document).ready(function(){
    let menuClick = $('#menuClick');
    let movilMenu = $('#movilMenu');

    menuClick.click(function(){
        movilMenu.slideToggle();
    });

    
    let cont = 0;
  
    let peon4w = document.querySelector('#wp4');
    let peon5w = document.querySelector('#wp5');  
    let cabaw1 = document.querySelector('#wc1');
    let cabaw2 = document.querySelector('#wc2');
    let alfw1 = document.querySelector('#wa1');
    let alfw2 = document.querySelector('#wa2');

   
    let peon4b = document.querySelector('#bp4');
    let peon5b = document.querySelector('#bp5');
    let cabab1 = document.querySelector('#bc1');
    let cabab2 = document.querySelector('#bc2');
    let alfb1 = document.querySelector('#ba1');
    let alfb2 = document.querySelector('#ba2');

    let piezas = [peon4w, peon4b, cabaw2, cabab2, cabaw1, cabab1, peon5w, peon5b, alfw2, alfb2, peon5w, peon5b, alfw1, alfb1];
    let piezasRow = [5, 4, 6, 3, 6, 3, 6, 3, 6, 3, 5, 4, 6, 3];
    let piezasColumn = [4, 4, 6, 6, 3, 3, 5, 5, 4, 4, 5, 5, 5, 5];
 

    let intervalo = setInterval(function(){  
        piezas[cont].style.gridRow = piezasRow[cont];
        piezas[cont].style.gridColumn = piezasColumn[cont];
        cont++;    
        
        if(cont>= piezas.length-1){
            clearInterval(intervalo);
        }
        
    }, 1000);
});